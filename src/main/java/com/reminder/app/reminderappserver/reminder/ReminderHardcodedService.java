package com.reminder.app.reminderappserver.reminder;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReminderHardcodedService {

    private static List<Reminder> reminders = new ArrayList<>();
    private static int idCounter = 0;

    public static void main(String[] args) {
        reminders.add(new Reminder(++idCounter, "Jalen", "Learn to Grow", new Date(), false));
        reminders.add(new Reminder(++idCounter, "Jalen", "Learn MICROSERVICES", new Date(), false));
        reminders.add(new Reminder(++idCounter, "Jalen", "Learn to ANGULAR", new Date(), false));

    }

    public List<Reminder> findAll() {
        return reminders;
    }

    public Reminder save(Reminder reminder) {
        if (reminder.getId() == -1 || reminder.getId() == 0) {
            reminder.setId(++idCounter);
        } else {
            deleteById(reminder.getId());
        }
        reminders.add(reminder);
        return reminder;
    }

    public Reminder deleteById(long id) {
        Reminder reminder = findById(id);

        if (reminder == null) return null;

        if (reminders.remove(reminder)) {
            return reminder;
        };

        return null;
    }

    public Reminder findById(long id) {
        for (Reminder reminder:reminders) {
            if (reminder.getId() == id) {
                return reminder;
            }
        }
        return null;
    }

}
