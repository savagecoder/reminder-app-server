package com.reminder.app.reminderappserver.reminder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ReminderResource {

    @Autowired
    private ReminderHardcodedService reminderService;

    @GetMapping("/users/{username}/reminders/{id}")
    public Reminder getReminder(@PathVariable String username, @PathVariable long id) {
        return reminderService.findById(id);
    }

    @GetMapping("/users/{username}/reminders")
    public List<Reminder> getAllReminders(@PathVariable String username) {
        return reminderService.findAll();
    }

    @DeleteMapping("/users/{username}/reminders/{id}")
    public ResponseEntity<Reminder> deleteReminderById(@PathVariable String username, @PathVariable long id) {
        Reminder reminder = reminderService.deleteById(id);
        if (reminder != null) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.notFound().build();
    }

    @PutMapping("/users/{username}/reminders/{id}")
    public ResponseEntity<Reminder> updateReminder(@PathVariable String username, @PathVariable long id, @RequestBody Reminder reminder) {
        Reminder reminderUpdated = reminderService.save(reminder);
        return new ResponseEntity<Reminder>(reminderUpdated, HttpStatus.OK);
    }

    @PostMapping("/users/{username}/reminders")
    public ResponseEntity<Void> createReminder(@PathVariable String username, @RequestBody Reminder reminder) {
        Reminder createdReminder = reminderService.save(reminder);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(createdReminder.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }
}
