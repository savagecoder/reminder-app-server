package com.reminder.app.reminderappserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReminderAppServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReminderAppServerApplication.class, args);
	}

}
